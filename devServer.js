/* eslint-env node */
/* eslint-disable no-console */

var express = require('express')
var webpack = require('webpack')
var config = require('./webpack.config.dev')
var proxy = require('express-http-proxy')

var app = express()
var compiler = webpack(config)

var coreUrlLocal = process.env.CORE_URL_LOCAL || 'http://localhost:8000'
var coreUrlApi = process.env.CORE_URL_API || 'http://localhost:8000'

console.log('Will proxy front-end url: ' + coreUrlLocal)
console.log('Will proxy api url: ' + coreUrlApi)

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}))

app.use(require('webpack-hot-middleware')(compiler))

var localProxy = proxy(coreUrlLocal, {
    forwardPath: function(req) {
        return req.originalUrl
    }
})

var apiProxy = proxy(coreUrlApi, {
    forwardPath: function(req) {
        return req.originalUrl
    }
})

app.use('*', function(req, res, next) {
    if (req.baseUrl.startsWith('/api')) {
        return apiProxy(req, res, next)
    } else {
        return localProxy(req, res, next)
    }
})

app.listen(3001, 'localhost', function(err) {
    if (err) {
        console.log(err)
        return
    }

    console.log('** Core Dev Server **')
    console.log('Listening at: http://localhost:3001')
    console.log('Building initial webpack (10-15s)...')
})
