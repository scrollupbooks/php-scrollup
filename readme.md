For installing php-v8js

```
sudo add-apt-repository ppa:pinepain/libv8-5.2
sudo apt-get update
sudo apt-get install libv8-5.2

cd /tmp
git clone https://github.com/phpv8/v8js.git
cd v8js
phpize
./configure
make
make test
sudo make install
```