<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>proteomx - @yield('title')</title>
    <link rel="stylesheet" type="text/css" href="/build/main.css" />
</head>
<body data-spy="scroll" data-target="#protein-scrollspy">
    @yield('content')
    @yield('urls')
</body>
</html>
