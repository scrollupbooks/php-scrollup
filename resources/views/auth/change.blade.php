@extends('layouts.master')
@section('title', 'Change Password')


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-16 col-sm-offset-10">
            <h2>Change Password</h2>
            @if($errors->any())
            <div class="alert alert-warning" role="alert">{{$errors->first()}}</div>
            @endif
            <form class="form-horizontal" method="POST" action="{{ URL('user/change-password') }}">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="old_password" class="col-sm-10 control-label">Current Password</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="password" id="old_password" name="old_password" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="user_password" class="col-sm-10 control-label">New Password</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="password" id="user_password" name="password" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="password_confirmation" class="col-sm-10 control-label">Confirm New Password</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-26 col-sm-offset-10">
                        <button type="submit" class="btn btn-primary">Change Password</button>
                    </div>
                </div>

                <div>

                </div>
            </form>
        </div>
    </div>
</div>
@endsection
