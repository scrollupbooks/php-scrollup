@extends('layouts.master')
@section('title', 'Reset Password')


@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-16 col-sm-offset-10">
            <h2>Reset Password</h2>
            @if($errors->any())
            <div class="alert alert-warning" role="alert">{{$errors->first()}}</div>
            @endif
            <form class="form-horizontal" method="POST" action="{{ URL('password/reset') }}">
                {!! csrf_field() !!}

                <input type="hidden" name="token" value="{{ $token }}">

                <div class="form-group">
                    <label for="user_email" class="col-sm-10 control-label">Email</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="email" id="user_email" name="email" value="{{ old('email') }}" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="user_password" class="col-sm-10 control-label">Password</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="password" id="user_password" name="password" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="password_confirmation" class="col-sm-10 control-label">Confirm Password</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="password" name="password_confirmation" id="password_confirmation" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-26 col-sm-offset-10">
                        <button type="submit" class="btn btn-primary">Reset Password</button>
                    </div>
                </div>

                <div>

                </div>
            </form>
        </div>
    </div>
</div>
@endsection
