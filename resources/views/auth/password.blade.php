@extends('layouts.master')
@section('title', 'Password Reset')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-16 col-sm-offset-10">
            <h2>Reset Password</h2>
            @if($errors->any())
                <div class="alert alert-warning" role="alert">{{$errors->first()}}</div>
            @endif
            @if(! $errors->any() &&  session('status') != "")
                <div class="alert alert-success" role="alert">{{session('status')}}</div>
            @endif
            <form class="form-horizontal"  method="POST" action="{{ URL('password/email') }}">
                {!! csrf_field() !!}

                <div class="form-group">
                    <label for="user_email" class="col-sm-10 control-label">Email</label>
                    <div class="col-sm-26">
                        <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-26 col-sm-offset-10">
                        <button class="btn btn-primary" type="submit">Send Password Reset Link</button>
                        <a class="btn btn-link" href="{{URL('auth/login')}}">Login</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
