<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        @if (!App::isLocal())
            <link rel="stylesheet" type="text/css" href="/build/main.css" />
        @endif
    </head>
    <body>
        <div id="root" class="root"></div>
        <script src="/build/bundle.js"></script>
    </body>
</html>
