/**
 * Actions
 */

import axios from 'axios'
import Immutable from 'immutable'
import url from 'url'
import _ from 'lodash'
import cookies from 'js-cookie'
import {
    UPDATE_LOCATION as RRRUL,
    TRANSITION as RRRT,
    routeActions as routeActionsImport
} from 'react-router-redux'

import { apiBase, xsrfCookieName, xsrfHeaderName } from '../config'

import { ifoMatchesSearch } from '../utils'

/**
 * Route actions alias
 */

export const UPDATE_LOCATION = RRRUL
export const TRANSITION = RRRT
export const routeActions = routeActionsImport

/**
 * API Utils
 */

/**
 * Make an HTTP request, track loading state,
 * and return a promise
 *
 * @param  {string}       method   The HTTP method (get, post, etc)
 * @param  {function}     dispatch The store/action dispatch function (optional)
 * @param  {array}        endpoint Endpoint or array of endpoint subpaths
 * @param  {object}       query    Object of query params (optional)
 * @param  {object}       query    Object of request data (optional)
 * @param  {object}       config   Object for axios config (optional)
 * @return {Promise}
 */
const request = (method, dispatch, endpoint, query, data, config) => {
    if (!_.isPlainObject(query)) {
        query = {}
    }
    if (!_.isPlainObject(data)) {
        data = {}
    }
    if (!_.isPlainObject(config)) {
        config = {}
    }

    // If endpoint isn't represented as an array, make it so
    if (!_.isArray(endpoint)) {
        endpoint = [endpoint]
    }

    // Prepend the api base
    endpoint.unshift(apiBase)
    // Cast elements to strings and make them safe
    endpoint = endpoint.map(String).map(encodeURIComponent)

    // Final URL
    const finalUrl = url.format({
        pathname: '/' + endpoint.join('/'),
        query: query
    })

    const hash = (new Date()).getTime() + String(Math.random())
    dispatch(receiveProgressNew(hash))

    // Make our final config
    _.assign(config, {
        method,
        data,
        xsrfCookieName,
        xsrfHeaderName,
        url: finalUrl,
        onProgress: e => {
            const percentage = e.lengthComputable
                ? _.clamp(_.ceil((e.loaded / e.total) * 100), 0, 100)
                : 100
            dispatch(receiveProgressUpdate(hash, percentage))
        }
    })

    // Make request
    const promise = axios(config)

    // Check if we have dispatch
    if (typeof dispatch === 'function') {
        // Promise done callback
        const done = response => {
            dispatch(receiveProgressUpdate(hash, 100))

            if (response instanceof Error) {
                throw response
            }

            return response
        }
        // Return promise with callbacks on success and failure
        return promise.then(done, done)
    } else {
        return promise
    }
}

// Partial request function applications for different verbs
const get = _.partial(request, 'get')

/**
 * Loading Indicator
 */

export const RECEIVE_PROGRESS_NEW = 'RECEIVE_PROGRESS_NEW'

export const receiveProgressNew = payload =>
    ({ type: RECEIVE_PROGRESS_NEW, payload })

export const RECEIVE_PROGRESS_UPDATE = 'RECEIVE_PROGRESS_UPDATE'

export const receiveProgressUpdate = (id, percentage) =>
    ({ type: RECEIVE_PROGRESS_UPDATE, payload: { id, percentage } })

/**
* Equipment Search Results
*/

export const REQUEST_EQUIPMENT_SEARCH_RESULTS = 'REQUEST_EQUIPMENT_SEARCH_RESULTS'

const requestEquipmentSearchResults = () =>
    ({ type: REQUEST_EQUIPMENT_SEARCH_RESULTS })

export const RECEIVE_EQUIPMENT_SEARCH_RESULTS = 'RECEIVE_EQUIPMENT_SEARCH_RESULTS'

const receiveEquipmentSearchResults = results =>
    ({
        type: RECEIVE_EQUIPMENT_SEARCH_RESULTS,
        payload: Immutable.fromJS(results)
    })

export const fetchEquipmentSearchResults = (searchQuery, searchFilter, searchType, resultsPerPage = 25, pageNumber = 1) =>
    dispatch => {
        dispatch(requestEquipmentSearchResults())

        get(dispatch, ['equipment', searchType, searchQuery, resultsPerPage, searchFilter], { page: pageNumber })
        .then(response => {
            dispatch(receiveEquipmentSearchResults(response.data))
        })
    }

export const CLEAR_EQUIPMENT_SEARCH_RESULTS = 'CLEAR_EQUIPMENT_SEARCH_RESULTS'

export const clearEquipmentSearchResults = () =>
    ({
        type: CLEAR_EQUIPMENT_SEARCH_RESULTS
    })

export const RECEIVE_SEARCHABLE_EQUIPMENT_FIELDS = 'RECEIVE_SEARCHABLE_EQUIPMENT_FIELDS'

export const receiveSearchableEquipmentFields = () =>
    dispatch => {
        get(dispatch, ['equipment', 'searchable-fields'])
        .then(response => {
            dispatch({
                type: RECEIVE_SEARCHABLE_EQUIPMENT_FIELDS,
                payload: response.data
            })
        })
    }

export const HOVER_EQUIPMENT_MAP_SEARCH_RESULTS = 'HOVER_EQUIPMENT_MAP_SEARCH_RESULTS'

export const hoverEquipmentMapSearchResults = (id) =>
    ({
        type: HOVER_EQUIPMENT_MAP_SEARCH_RESULTS,
        payload: id
    })

export const SELECT_BUILDING_IN_MAP_VIEW = 'SELECT_BUILDING_IN_MAP_VIEW'

export const selectBuildingInMapView = (buildingName, results) => 
    ({
        type: SELECT_BUILDING_IN_MAP_VIEW,
        payload: {buildingName: buildingName, results: results}
    })

/**
 * Chosen entity
 */

export const CHOOSE_ENTITY = 'CHOOSE_ENTITY'

export const chooseEntity = payload =>
    ({ type: CHOOSE_ENTITY, payload })

export const CLOSE_ENTITY = 'CLOSE_ENTITY'

export const closeEntity = payload =>
    ({ type: CLOSE_ENTITY, payload })

/**
 * Managing entities
 */

export const REQUEST_ENTITIES = 'REQUEST_ENTITIES'

export const requestEntities = () =>
    ({ type: REQUEST_ENTITIES })

export const RECEIVE_ENTITIES = 'RECEIVE_ENTITIES'

export const receiveEntities = payload =>
    ({ type: RECEIVE_ENTITIES, payload })

export const fetchEntities = () =>
    dispatch => {
        dispatch(requestEntities())

        get(dispatch, ['entity', 'all'])
        .then(response => {
            const { managing_entities } = response.data

            const entities = Immutable.fromJS(managing_entities)

            dispatch(receiveEntities(entities))

            // Reduce all facilities
            const facilities = entities.reduce((acc, ent) =>
                ent.has('facilities')
                    ? acc.concat(ent.get('facilities'))
                    : acc,
            Immutable.List())

            // Reduce all facility contacts
            const contacts = facilities.reduce((acc, facility) =>
                facility.has('contacts')
                    ? acc.concat(facility.get('contacts'))
                    : acc,
            Immutable.List())

            // receive staff contacts (for the facility box flip sides)
            dispatch(receiveFacilityParts({ type: 'contacts', parts: contacts }))

            // receive facilities
            dispatch(receiveFacilities(facilities))
        })
    }

/**
 * Chosen facility
 */

export const CHOOSE_FACILITY = 'CHOOSE_FACILITY' // not used

export const chooseFacility = id =>
    dispatch => {
        // Fetch it, and choose later
        dispatch(fetchFacility(id, 'id', true))
    }

export const CHOOSE_FACILITY_BY_ALIAS = 'CHOOSE_FACILITY_BY_ALIAS' // not used

export const chooseFacilityByAlias = alias =>
    dispatch => {
        // Fetch it, and choose later
        dispatch(fetchFacility(alias, 'alias', true))
    }

export const CLOSE_FACILITY = 'CLOSE_FACILITY'

export const closeFacility = () =>
    dispatch => {
        // Unlock the window scrollbar
        dispatch(windowLockScroll(false))

        // Close rates type prompt if it's being shown
        dispatch(showRatesTypePrompt(false))

        // Update URL
        dispatch(routeActions.push('/resources'))

        // The actual action
        dispatch({ type: CLOSE_FACILITY })
    }

/**
 * Facilities
 */

export const REQUEST_FACILITIES = 'REQUEST_FACILITIES'

export const requestFacilities = () =>
    ({ type: REQUEST_FACILITIES })

export const RECEIVE_FACILITIES = 'RECEIVE_FACILITIES'

export const receiveFacilities = payload =>
    ({ type: RECEIVE_FACILITIES, payload: payload })

// idType can be 'id' or 'alias'
export const fetchFacility = (facilityId, idType = 'id', chooseIt = false) =>
    dispatch => {
        dispatch(requestFacilities())

        const queryParams = idType === 'alias' ? {
            url_alias: facilityId
        } : undefined

        get(dispatch, ['facility', facilityId], queryParams)
        .then(response => {
            const { facility } = response.data

            const imFacility = Immutable.fromJS(facility)

            const imFacilityWithoutSections = imFacility
                .delete('sections')
                .delete('contacts')
                .delete('power_users')
                .delete('services')

            dispatch(receiveFacilities([imFacilityWithoutSections]))
            dispatch(receiveFacilityParts({ type: 'sections', parts: imFacility.get('sections') }))
            dispatch(receiveFacilityParts({ type: 'contacts', parts: imFacility.get('contacts') }))
            dispatch(receiveFacilityParts({ type: 'power_users', parts: imFacility.get('power_users') }))
            dispatch(receiveFacilityParts({ type: 'services', parts: imFacility.get('services') }))

            const actualId = imFacility.get('id')

            if (chooseIt) {
                // The actual action
                dispatch({ type: CHOOSE_FACILITY, payload: actualId })
                // Lock the scroll
                dispatch(windowLockScroll(true))
                // Update URL
                dispatch(routeActions.push(`/resources/${facilityId}`))
            }
        })
        .catch(response => {
            console.log('fetchFacility promise exception', response)
        })
    }

/**
 * Facility search
 */

export const RESET_FACILITIES_SEARCH = 'RESET_FACILITIES_SEARCH'

export const resetFacilitiesSearch = () =>
    ({ type: RESET_FACILITIES_SEARCH })

export const RECEIVE_FACILITIES_SEARCH = 'RECEIVE_FACILITIES_SEARCH'

export const receiveFacilitiesSearch = payload =>
    ({ type: RECEIVE_FACILITIES_SEARCH, payload })

export const searchFacilities = query =>
    dispatch => {
        get(dispatch, ['facility', 'search', query])
        .then(response => {
            const facilities = response.data

            if (!Array.isArray(facilities)) {
                throw new Error('bad facility ids')
            }

            const imFacilities = Immutable.fromJS(facilities).reduce(
                (acc, f) => acc.set(_.parseInt(f.get('id')), f.get('score')),
                Immutable.Map())

            dispatch(receiveFacilitiesSearch(imFacilities))
        })
        .catch(err => {
            console.log('searchFacilities exception', err)
        })
    }

/**
 * Facility Sections
 */

export const RECEIVE_FACILITY_PARTS = 'RECEIVE_FACILITY_PARTS'

export const receiveFacilityParts = payload =>
    ({ type: RECEIVE_FACILITY_PARTS, payload, receivedAt: Date.now() })

/**
 * Facility Services Expand/Collapse Specs
 */

export const EXPAND_SERVICE = 'EXPAND_SERVICE'

export const expandService = payload =>
    ({ type: EXPAND_SERVICE, payload })

export const COLLAPSE_SERVICE = 'COLLAPSE_SERVICE'

export const collapseService = payload =>
    ({ type: COLLAPSE_SERVICE, payload })

/**
 * Facility Services Expand/Collapse Rates
 */

export const EXPAND_RATES = 'EXPAND_RATES'

export const expandRates = payload =>
    (dispatch, getState) => {
        const state = getState()
        if (!state.ratesTypePrompt && !state.ratesType) {
            // no rates type selected and prompt not
            // currently being shown; so show prompt
            dispatch(showRatesTypePrompt(true))
        }

        // the actual action
        dispatch({ type: EXPAND_RATES, payload })
    }

export const COLLAPSE_RATES = 'COLLAPSE_RATES'

export const collapseRates = payload =>
    ({ type: COLLAPSE_RATES, payload })

/**
 * Facility services rates type
 */

export const RECEIVE_RATES_TYPE = 'RATES_TYPE'

export const receiveRatesType = payload => {
    cookies.set('uconn-core-rates-type', payload)

    return { type: RECEIVE_RATES_TYPE, payload }
}

/**
 * Rates type prompt
 */

export const SHOW_RATES_TYPE_PROMPT = 'SHOW_RATES_TYPE_PROMPT'

export const showRatesTypePrompt = payload =>
    ({ type: SHOW_RATES_TYPE_PROMPT, payload })

/**
 * Window
 */

export const WINDOW_LOCK_SCROLL = 'WINDOW_LOCK_SCROLL'

export const windowLockScroll = payload =>
    ({ type: WINDOW_LOCK_SCROLL, payload })

/**
 * Internal Funding Opportunities (IFOs)
 */

export const REQUEST_IFOS = 'REQUEST_IFOS'

export const requestIfos = () =>
    ({ type: REQUEST_IFOS })

export const RECEIVE_IFOS = 'RECEIVE_IFOS'

export const receiveIfos = payload =>
    ({ type: RECEIVE_IFOS, payload })

export const fetchIfos = () =>
    dispatch => {
        dispatch(requestIfos())

        get(dispatch, ['ifo', 'all'])
        .then(response => {
            const { internal_funding_opportunities } = response.data
            const ifos = Immutable.fromJS(internal_funding_opportunities)
            dispatch(receiveIfos(ifos))
        })
    }

/**
 * Chosen IFOs
 */

export const CHOOSE_IFO = 'CHOOSE_IFO'

export const chooseIfo = payload =>
    ({ type: CHOOSE_IFO, payload })

export const CLOSE_IFO = 'CLOSE_IFO'

export const closeIfo = payload =>
    ({ type: CLOSE_IFO, payload })

export const CLOSE_ALL_IFO = 'CLOSE_ALL_IFO'

export const closeAllIfos = payload =>
    ({ type: CLOSE_ALL_IFO, payload })

/**
 * IFO search
 */

export const RESET_IFOS_SEARCH = 'RESET_IFOS_SEARCH'

export const resetIfosSearch = () =>
    ({ type: RESET_IFOS_SEARCH })

export const RECEIVE_IFOS_SEARCH = 'RECEIVE_IFOS_SEARCH'

export const receiveIfosSearch = payload =>
    ({ type: RECEIVE_IFOS_SEARCH, payload })

export const searchIfos = query =>
    (dispatch, getState) => {
        // TODO see searchFacilities for decent way to make actual request
        // right now, just do a plain ifo text lookup to fake it

        const { ifos } = getState()

        const results = ifos
            .filter(_.partial(ifoMatchesSearch, query))
            .reduce((acc, f) => acc.set(f.get('id'), 0), // zero score for now
                    Immutable.Map())

        dispatch(receiveIfosSearch(results))
    }
