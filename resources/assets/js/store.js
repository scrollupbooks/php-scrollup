/**
 * Store atom (singleton) with middleware
 */

import { createStore, applyMiddleware, compose } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { browserHistory } from 'react-router'
import { syncHistory } from 'react-router-redux'
import reducers from './reducers'

// Middleware to sync created history with the store
const routerMiddleware = syncHistory(browserHistory)

export default createStore(reducers, undefined, compose(
    applyMiddleware(
        thunkMiddleware, // lets us dispatch() functions
        routerMiddleware // puts router state in store
    ),
    window.devToolsExtension ? window.devToolsExtension() : f => f
))
