import { EventEmitter } from 'events'
import $ from 'jquery'

const CHANGE_EVENT = 'change'

class notification extends EventEmitter {
    constructor() {
        super()
        this.nextId = 0
        this.reset()
    }

    emitChange() {
        this.emit(CHANGE_EVENT)
    }

    addChangeListener(callback) {
        this.on(CHANGE_EVENT, callback)
    }

    removeChangeListener(callback) {
        this.removeListener(CHANGE_EVENT, callback)
    }

    push(type, message) {
        this.bag.push({
            id: this.nextId,
            type: type,
            message: message
        })

        this.emitChange()

        return this.nextId++
    }

    resetAndPush(type, message) {
        this.reset()
        this.push(type, message)
    }

    /**
     * Remove notification with given id
     */
    remove(id) {
        id = parseInt(id)
        this.bag = this.bag.filter(notif => {
            return notif.id !== id
        })
    }

    reset() {
        this.bag = []
        this.emitChange()
    }

    getBag() {
        return this.bag
    }

    scrollTo() {
        $('html, body').animate({
            scrollTop: $('#notificationSection').offset().top - 15
        }, 500)
    }
}

// Singleton
export default new notification()
