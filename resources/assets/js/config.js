export const apiBase = 'api'
export const xsrfCookieName = 'XSRF-TOKEN'
export const xsrfHeaderName = 'X-XSRF-TOKEN'
export const NONE_SELECTED = 'NONE_SELECTED'
