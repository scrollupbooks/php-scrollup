import { RECEIVE_RATES_TYPE } from '../actions'
import cookies from 'js-cookie'
import _ from 'lodash'

export const getStoredState = () => {
    const stored = cookies.get('uconn-core-rates-type')

    return _.includes(['internal', 'external'], stored)
        ? stored
        : null
}

const initialState = getStoredState()

export default (state = initialState, action) =>
    action.type === RECEIVE_RATES_TYPE ? action.payload : state
