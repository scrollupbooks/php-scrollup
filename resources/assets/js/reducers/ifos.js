import Immutable from 'immutable'
import { RECEIVE_IFOS } from '../actions'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_IFOS:
        // Replace old with new
        return state
            .filter(ifo =>
                action.payload
                    .every(re => re.get('id') !== ifo.get('id')))
            .concat(action.payload)
    default:
        return state
    }
}
