import Immutable from 'immutable'
import { RECEIVE_FACILITIES } from '../actions'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_FACILITIES:
        // Replace old with new
        return state
            .filter(facility =>
                action.payload
                    .every(rf => rf.get('id') !== facility.get('id')))
            .concat(action.payload)
    default:
        return state
    }
}
