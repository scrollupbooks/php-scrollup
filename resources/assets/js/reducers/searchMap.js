import Immutable from 'immutable'
import { HOVER_EQUIPMENT_MAP_SEARCH_RESULTS } from '../actions'

const initialState = Immutable.Map({
    hoverID: null
})

export default (state = initialState, action) => {
    switch (action.type) {
    case HOVER_EQUIPMENT_MAP_SEARCH_RESULTS:
        return  Immutable.Map({
            hoverID: action.payload
        })
    default:
        return state
    }
}
