import { CHOOSE_FACILITY, CLOSE_FACILITY } from '../actions'

const initialState = null

export default (state = initialState, action) => {
    switch (action.type) {
    case CHOOSE_FACILITY:
        return action.payload
    case CLOSE_FACILITY:
        return null
    default:
        return state
    }
}
