import { RECEIVE_FACILITIES_SEARCH, RESET_FACILITIES_SEARCH } from '../actions'

const initialState = null

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_FACILITIES_SEARCH:
        return action.payload
    case RESET_FACILITIES_SEARCH:
        return initialState
    default:
        return state
    }
}
