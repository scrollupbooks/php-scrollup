/**
 * Root reducer file
 */

import { combineReducers } from 'redux'

import { routeReducer as routing } from 'react-router-redux'

import { reducer as form } from 'redux-form'

import chosenEntities from './chosenEntities'
import chosenFacility from './chosenFacility'
import chosenIfos from './chosenIfos'
import entities from './entities'
import expandedServices from './expandedServices'
import expandedRates from './expandedRates'
import facilities from './facilities'
import facilityParts from './facilityParts'
import ifos from './ifos'
import progress from './progress'
import ratesType from './ratesType'
import ratesTypePrompt from './ratesTypePrompt'
import searchResults from './searchResults'
import searchDetails from './searchDetails'
import equipmentSearchInProgress from './equipmentSearchInProgress'
import searchMap from './searchMap'
import searchableFields from './searchableFields'
import searchFacilities from './searchFacilities'
import selectedMapViewBuilding from './selectedMapViewBuilding'
import searchIfos from './searchIfos'
import theWindow from './theWindow'

// Export combined reducers
export default combineReducers({
    routing,
    form,

    chosenEntities,
    chosenFacility,
    chosenIfos,
    entities,
    expandedServices,
    expandedRates,
    facilities,
    facilityParts,
    ifos,
    progress,
    ratesType,
    ratesTypePrompt,
    searchResults,
    searchDetails,
    equipmentSearchInProgress,
    searchMap,
    searchableFields,
    searchFacilities,
    selectedMapViewBuilding,
    searchIfos,
    theWindow
})
