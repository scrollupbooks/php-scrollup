import { SELECT_BUILDING_IN_MAP_VIEW, CLEAR_EQUIPMENT_SEARCH_RESULTS, RECEIVE_EQUIPMENT_SEARCH_RESULTS } from '../actions'
import Immutable from 'immutable'

const initialState = Immutable.fromJS({buidingName: null, results: []})

export default (state = initialState, action) => {
    switch (action.type) {
    case SELECT_BUILDING_IN_MAP_VIEW:
        return Immutable.fromJS(action.payload)
    case CLEAR_EQUIPMENT_SEARCH_RESULTS:
        return initialState
    case RECEIVE_EQUIPMENT_SEARCH_RESULTS:
        return initialState
    default:
        return state
    }
}
