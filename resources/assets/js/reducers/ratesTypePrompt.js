import { SHOW_RATES_TYPE_PROMPT } from '../actions'

const initialState = false

export default (state = initialState, action) =>
    action.type === SHOW_RATES_TYPE_PROMPT ? action.payload : state
