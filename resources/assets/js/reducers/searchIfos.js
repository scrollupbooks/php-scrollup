import { RECEIVE_IFOS_SEARCH, RESET_IFOS_SEARCH } from '../actions'

const initialState = null

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_IFOS_SEARCH:
        return action.payload
    case RESET_IFOS_SEARCH:
        return initialState
    default:
        return state
    }
}
