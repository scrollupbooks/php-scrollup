import { CHOOSE_IFO, CLOSE_IFO, CLOSE_ALL_IFO } from '../actions'
import Immutable from 'immutable'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case CHOOSE_IFO:
        return state.push(action.payload)
    case CLOSE_IFO:
        return state.filter(id => id !== action.payload)
    case CLOSE_ALL_IFO:
        return initialState
    default:
        return state
    }
}
