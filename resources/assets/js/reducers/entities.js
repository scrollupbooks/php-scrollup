import Immutable from 'immutable'
import { RECEIVE_ENTITIES } from '../actions'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_ENTITIES:
        // Replace old with new
        return state
            .filter(entity =>
                action.payload
                    .every(re => re.get('id') !== entity.get('id')))
            .concat(action.payload)
    default:
        return state
    }
}
