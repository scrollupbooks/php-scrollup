import { EXPAND_SERVICE, COLLAPSE_SERVICE } from '../actions'
import Immutable from 'immutable'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case EXPAND_SERVICE:
        return state.push(action.payload)
    case COLLAPSE_SERVICE:
        return state.filter(serviceId => serviceId !== action.payload)
    default:
        return state
    }
}
