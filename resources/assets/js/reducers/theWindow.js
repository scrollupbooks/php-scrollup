import Immutable from 'immutable'
import {
    WINDOW_LOCK_SCROLL
} from '../actions'

// NOTE see important note about potential source of
// lock-scroll bugs in the projectViewClose action creator

export const initialState = Immutable.fromJS({
    lockScroll: false
})

export default (state = initialState, action) => {
    switch (action.type) {
    case WINDOW_LOCK_SCROLL:
        return state.set('lockScroll', action.payload)
    default:
        return state
    }
}
