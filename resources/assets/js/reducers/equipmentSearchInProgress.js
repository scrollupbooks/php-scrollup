import Immutable from 'immutable'
import { REQUEST_EQUIPMENT_SEARCH_RESULTS, RECEIVE_EQUIPMENT_SEARCH_RESULTS } from '../actions'

const initialState = false

export default (state = initialState, action) => {
    switch (action.type) {
    case REQUEST_EQUIPMENT_SEARCH_RESULTS:
        return true
    case RECEIVE_EQUIPMENT_SEARCH_RESULTS:
        return false
    default:
        return state
    }
}
