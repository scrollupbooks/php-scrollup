import Immutable from 'immutable'
import { RECEIVE_PROGRESS_NEW, RECEIVE_PROGRESS_UPDATE } from '../actions'

const initialState = Immutable.fromJS({})

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_PROGRESS_NEW:
        return state.set(action.payload, 0)
    case RECEIVE_PROGRESS_UPDATE:
        const { id, percentage } = action.payload

        if (!state.has(id)) return state

        if (percentage === 100) {
            // Remove
            return state.delete(id)
        }

        // Update progress
        return state.set(id, percentage)
    default:
        return state
    }
}
