import { CHOOSE_ENTITY, CLOSE_ENTITY } from '../actions'
import Immutable from 'immutable'

const initialState = Immutable.fromJS([])

export default (state = initialState, action) => {
    switch (action.type) {
    case CHOOSE_ENTITY:
        return state.push(action.payload)
    case CLOSE_ENTITY:
        return state.filter(id => id !== action.payload)
    default:
        return state
    }
}
