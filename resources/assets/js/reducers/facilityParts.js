import Immutable from 'immutable'
import { RECEIVE_FACILITY_PARTS } from '../actions'

const initialState = Immutable.fromJS({
    sections: [],
    contacts: [],
    power_users: [],
    services: []
})

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_FACILITY_PARTS:
        const { type, parts } = action.payload
        const subState = state.get(type)

        // replace old with new
        return state.set(type, subState
            .filter(part =>
                parts.every(rp =>
                    rp.get('id') !== part.get('id')))
            .concat(parts))
    default:
        return state
    }
}
