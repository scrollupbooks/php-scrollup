import Immutable from 'immutable'
import { RECEIVE_SEARCHABLE_EQUIPMENT_FIELDS, CLEAR_EQUIPMENT_SEARCH_RESULTS } from '../actions'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_SEARCHABLE_EQUIPMENT_FIELDS:
        return  action.payload
    case CLEAR_EQUIPMENT_SEARCH_RESULTS:
        return initialState
    default:
        return state
    }
}
