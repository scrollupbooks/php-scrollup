import { EXPAND_RATES, COLLAPSE_RATES } from '../actions'
import Immutable from 'immutable'

const initialState = Immutable.List()

export default (state = initialState, action) => {
    switch (action.type) {
    case EXPAND_RATES:
        return state.push(action.payload)
    case COLLAPSE_RATES:
        return state.filter(serviceId => serviceId !== action.payload)
    default:
        return state
    }
}
