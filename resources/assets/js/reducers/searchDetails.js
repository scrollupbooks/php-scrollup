import Immutable from 'immutable'
import { RECEIVE_EQUIPMENT_SEARCH_RESULTS, CLEAR_EQUIPMENT_SEARCH_RESULTS } from '../actions'

const initialState = Immutable.Map({
    current_page: 0,
    last_page: 0,
    search_term: ''
})

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_EQUIPMENT_SEARCH_RESULTS:
        console.log('details reducer current page: ' + action.payload.get('details').get('current_page'))
        return  action.payload.get('details')
    default:
        return state
    }
}
