import Immutable from 'immutable'
import { RECEIVE_EQUIPMENT_SEARCH_RESULTS, CLEAR_EQUIPMENT_SEARCH_RESULTS } from '../actions'

const initialState = Immutable.fromJS([])

export default (state = initialState, action) => {
    switch (action.type) {
    case RECEIVE_EQUIPMENT_SEARCH_RESULTS:
        //console.log('adding results to')
        //console.log(state)        
        if (Immutable.Iterable.isIterable(action.payload) && action.payload.has('current_page') &&
            action.payload.get('current_page') <= action.payload.get('last_page')) {
            if (state.size != 0 && action.payload.get('current_page') != 1) {
                return state.concat(action.payload.get('hits'))
            }else {
                return action.payload.get('hits')
            }
        }
        return state
    case CLEAR_EQUIPMENT_SEARCH_RESULTS:
        //console.log('initialState:')
        //console.log(initialState)
        return initialState
    default:
        return state
    }
}
