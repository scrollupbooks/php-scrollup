import React from 'react'
import { connect } from 'react-redux'
import { typeTheWindow } from '../types'
import $ from 'jquery'

import Navbar from '../components/navbar'

// Top-level sass
import '../../sass/app.scss'

class MainApp extends React.Component {
    static propTypes = {
        theWindow: typeTheWindow.isRequired
    };

    componentDidMount() {
        this.decideBodyClass()
    }

    componentDidUpdate() {
        this.decideBodyClass()
    }

    decideBodyClass() {
        // Lock body ('theWindow' is a bit of a misnomer) depending on
        // lockScroll state
        $(document.body).toggleClass('lock-scroll', this.props.theWindow.get('lockScroll'))
    }
    
    render() {
        const { children } = this.props

        return (
            <div className="main-app">
                <Navbar />
                <div className="main-content">
                    {children}
                </div>
            </div>
        )
    }
}

const select = state => {
    return {
        theWindow: state.theWindow
    }
}

export default connect(select)(MainApp)
