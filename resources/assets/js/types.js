/**
 * Types
 *
 * Put all prop types into this file and export.
 * Makes it easier to compose types, ex. typeProjects is list of typeProject
 *
 * Do not use isRequired at the highest level (on the outer exported type);
 * leave that up to the component
 */

import { PropTypes } from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes'

export const typeBool = PropTypes.bool

export const typeNumber = PropTypes.number

export const typeString = PropTypes.string

export const typeFunc = PropTypes.func

export const typeObject = PropTypes.object

export const typeElement = PropTypes.element

export const typeNode = PropTypes.node

export const typeRouting = PropTypes.shape({
    changeId: typeNumber,
    path: typeString
})

// TODO more accuracy
export const typeLoaderPromises = ImmutablePropTypes.listOf(PropTypes.object)

export const typeTheWindow = ImmutablePropTypes.contains({
    lockScroll: typeBool.isRequired
})

export const typeEntity = ImmutablePropTypes.contains({
    id: typeNumber.isRequired,
    name: typeString.isRequired,
    // image: typeString.isRequired,
    about: typeString.isRequired
})

export const typeEntities = ImmutablePropTypes.listOf(typeEntity)

export const typeChosenEntities = ImmutablePropTypes.listOf(typeNumber)

export const typeFacility = ImmutablePropTypes.contains({
    id: typeNumber.isRequired,
    name: typeString.isRequired
})

export const typeFacilities = ImmutablePropTypes.listOf(typeFacility)
