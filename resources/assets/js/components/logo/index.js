import React, {Component} from 'react'
import classNames from 'classnames'
import './styles.scss'

export default class Logo extends Component {

    render() {
        const {size} = this.props

        let classes = {
            'logo': true
        }

        let width, height

        if (size === 'sm') {
            width = '32px'
            height = '32px'
        }else if (size === 'md') {
            width = '50px'
            height = '50px'
        }else if (size === 'lg') {
            width = '250px'
            height = '250px'
        }else {
            width = size
            height = size
        }

        return (
            <svg
               className={ classNames(classes) }
               width={ width }
               height={ height }
               viewBox="0 0 272.83465 272.83464"
               version="1.1">
              <g
                transform="translate(-83.507641,3.5659209)">
                <path
                  d="M 134.27087,74.076765 339.90005,34.232522 c 5.00001,-0.892856 6.89411,0.981834 6.89513,4.277833 l 0.056,180.730655 -44.62357,8.62438 -1.04206,41.77312 c 0,0 -1.5596,9.44686 -9.54475,2.44924 L 254.50625,236.8865 131.51304,260.67667 c -12.11695,-0.78031 -18.50096,-2.98485 -25.04198,-7.46603 -8.567237,-6.00137 -12.385666,-17.66362 -13.430755,-21.27773 L 92.998749,48.594834 c 0.08349,-2.3892 0.916388,-2.954695 4.76117,-3.666816 L 301.89506,5.6191161 c 4.50476,-0.8731665 6.41902,0.3478363 6.50831,4.0085505 l -0.2779,6.8703374 c 0,0 -0.14687,1.215695 -1.08512,1.602712 L 107.2088,56.482334 c -2.77791,0.63135 -3.52491,1.055795 -3.52492,2.697295 2.81759,11.620603 19.0442,16.058936 30.58699,14.897136 z"
                  className="book" />
              </g>
            </svg>
        )
    }

}
