import React, {Component} from 'react'
import classNames from 'classnames'
import {navbar, brand} from './styles.scss'
import Logo from '../logo'

export default class Navbar extends Component {

    render() {
        const {fixedToTop} = this.props

        console.log(navbar)

        let classes = {
            'navbar': true,
            'navbar-dark': true,
            'bg-inverse': true,
            'navbar-fixed-top': fixedToTop,
        }

        classes[navbar] = true

        return (
            <nav className={ classNames(classes) }>
                <ul className="nav navbar-nav">                    
                    <a className={classNames('navbar-brand', brand)} href="#"><Logo size="sm" /></a>
                    <li className="nav-item active">
                        <a className="nav-link" href="#">
                        Home <span className="sr-only">(current)</span></a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Features</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">Pricing</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link" href="#">About</a>
                    </li>
                </ul>
            </nav>
        )
    }

}
