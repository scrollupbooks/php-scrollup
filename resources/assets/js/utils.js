/**
 * Utils
 */

import _ from 'lodash'

export function includeTwitterWidgets() {
    return new Promise(resolve => {
        const scriptId = 'twitter-widgets-included'

        if (document.getElementById(scriptId)) {
            resolve(window.twttr)
        } else {
            const script = document.createElement('script')
            script.type = 'text/javascript'
            script.id = scriptId
            script.onload = () => {
                // Cleanup onload handler
                script.onload = null

                // Resolve promise
                resolve(window.twttr)
            }

            // Add script to the DOM
            (document.getElementsByTagName('head')[0]).appendChild(script)

            // Set the `src` to begin transport
            script.src = '//platform.twitter.com/widgets.js'
        }
    })
}

export function ifoMatchesSearch(search, ifo) {
    if (_.isEmpty(search)) {
        return false
    }

    const lowercasedSearch = _.lowerCase(search)

    const tryFields = ['deadlines', 'description', 'dollar_amount', 'entity', 'title']

    return tryFields.some(field => {
        const v = ifo.get(field)
        return !_.isEmpty(v) && _.includes(_.lowerCase(v), lowercasedSearch)
    })
}

export function coreNameHtmlSub(str) {
    return _.replace(str, /CORE/g, 'COR<sup class="core-sup">2</sup>E')
}
