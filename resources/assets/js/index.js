/**
 * JS Root File
 */

import 'babel-polyfill'

import 'autotrack'

import { render } from 'react-dom'
import routes from './routes'
import store from './store'

// Render routes into root div
render(routes(store), document.getElementById('root'))
