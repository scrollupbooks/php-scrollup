import jsdom from 'jsdom'
import chai from 'chai'
import TestUtils from 'react-addons-test-utils'

// A super simple DOM ready for React to render into
// Store this DOM and the window in global scope ready for React to access
global.document = jsdom.jsdom('<!doctype html><html><body></body></html>')
global.window = document.defaultView // or parentWindow?
global.navigator = { userAgent: 'node.js' }

// Expose chai's assert/expect/should as globals
global.assert = chai.assert
global.expect = chai.expect
global.should = chai.should() // extends Object.prototype

// Expose TestUtils as a global
global.TestUtils = TestUtils
