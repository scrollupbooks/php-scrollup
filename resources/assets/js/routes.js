/**
 * Routes
 */

import React from 'react' // eslint-disable-line
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { Provider } from 'react-redux'

import {    
    Home,
    MainApp,
    Design
} from './containers'

// The routes
const routes = (
    <div>
        {/* Front facing routes */}
        <Route path="/" component={MainApp}>
            <IndexRoute component={Home}/>
            <Route path="design" component={Design} />
            {/* Not Found */}
            <Route path="*" component={Home} notfound />
        </Route>
    </div>
)

export default store =>
    <Provider store={store}>
        <Router history={browserHistory} routes={routes} />
    </Provider>
