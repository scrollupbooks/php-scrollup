<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('{all}', function () {
    return view('welcome');
});


Route::group(['prefix'=>'design','middleware' => 'auth'], function () {
    Route::get('{all}', function ()    {
        return view('test');
    });
});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {

});

$api->version('v1', function ($api) {
    $api->get('auth/login', ['middleware' => 'api.auth', function () {
        // This route requires authentication.
    }]);

    $api->get('auth/login/{email}/{password}', 'StatelessAuthController@authenticate');
});